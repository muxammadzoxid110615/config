
from django.shortcuts import render, redirect
from .models import Column, Client, Direction


def sales_manager(request):
    columns = Column.objects.all()
    clients = Client.objects.all()
    print(clients)  # Добавим вывод для отладки
    return render(request, 'sales_manager_panel.html', {'columns': columns, 'clients': clients})


def add_client(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        surname = request.POST.get('surname')
        phone_number = request.POST.get('phone_number')
        sales_channel = request.POST.get('sales_channel')
        course = request.POST.get('course')
        birth_date = request.POST.get('birth_date')

        new_client = Client.objects.create(
            name=name,
            surname=surname,
            phone_number=phone_number,
            sales_channel=sales_channel,
            course=course,
            birth_date=birth_date
        )

        return redirect('sales_manager:sales_manager')

    return render(request, 'sales_manager_panel.html')


def your_view(request):
    directions = Direction.objects.all()
    return render(request, 'sales_manager_panel.html', {'direction': directions})