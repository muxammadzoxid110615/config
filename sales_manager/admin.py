from django.contrib import admin

from .models import Client, Column, Direction

# Register your models here.

admin.site.register(Client)

admin.site.register(Column)


admin.site.register(Direction)