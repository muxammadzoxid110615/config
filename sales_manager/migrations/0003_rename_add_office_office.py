# Generated by Django 4.2.6 on 2023-11-24 17:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sales_manager', '0002_add_office'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='add_OFFICE',
            new_name='Office',
        ),
    ]
