
from django.urls import path
from .views import sales_manager, add_client

urlpatterns = [
    path('', sales_manager, name='sales_manager'),
    path('add_client/', add_client, name='add_client'),  # Это имя маршрута
]