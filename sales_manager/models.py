from django.db import models


class Client(models.Model):
    name = models.CharField(max_length=255)
    surname = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=15)
    sales_channel = models.CharField(max_length=255)
    course = models.CharField(max_length=255)
    birth_date = models.DateField()


class Column(models.Model):
    name = models.CharField(max_length=255)
    clients = models.ManyToManyField(Client, related_name='columns', blank=True)


class Direction (models.Model):
    name = models.CharField(max_length=255, unique=True)
    color = models.CharField(max_length=7, default='#000000')

